'use strict';

document.addEventListener('DOMContentLoaded', () => {
  const start = document.querySelector('.app__start-button');
  const counter = document.querySelector('.app__counter');
  const feed = document.querySelector('.app__feed');
  const apply = document.querySelector('.app__apply-button');

  start.addEventListener('click', () => {
    const startClasses = start.getAttribute('class').split(/\s+/);
    startClasses.push('button_hidden');
    start.setAttribute('class', startClasses.join(' '));

    const socket = io();

    socket.on('connect', () => {
      feed.innerHTML = 'Соединение успешно установлено';
    });

    let isApplyHidden = true;

    socket.on('cashMaked', value => {
      counter.innerHTML = value;
      feed.innerHTML = '';

      if (value && isApplyHidden) {
        isApplyHidden = false;

        const applyClasses = apply.getAttribute('class').split(/\s+/);

        apply.setAttribute(
          'class',
          applyClasses
            .filter(currentClass => currentClass !== 'button_hidden')
            .join(' ')
        );
      }
    });

    socket.on('cashDecline', () => {
      feed.innerHTML = 'Ошибка распозанавания банкноты';
    });
  });
});
