'use strict';

const path = require('path');

const gulp = require('gulp');
const less = require('gulp-less');
const concat = require('gulp-concat');
const LessAutoprefix = require('less-plugin-autoprefix');

const webpackStream = require('webpack-stream');

const lessOptions = { plugins: [
  new LessAutoprefix([
    'last 5 Chrome versions',
    'last 5 Firefox versions',
    'iOS >= 8',
    'ie >= 9',
  ]),
] };

const clientSrcDir = path.join(__dirname, 'client');
const clientSrcJsPath = path.join(clientSrcDir, 'index.js');

const clientDestDir = path.join(__dirname, 'build');
const clientJsDestDir = path.join(clientDestDir, 'js');


const normalizeCssPath = path.join(
  __dirname, 'node_modules', 'normalize.css', 'normalize.css'
);

const mainLessPath = path.join(clientSrcDir, 'styles.less');

gulp.task('styles', () =>
  gulp.src([normalizeCssPath, mainLessPath])
  .pipe(concat('styles.css'))
  .pipe(less(lessOptions))
  .pipe(gulp.dest(clientDestDir))
);

const webpackConfig = {
  output: {
    filename: 'index.js',
  },
  module: { loaders: [{
    loader: 'babel',
    query: {
      presets: ['es2015', 'es2016'],
      env: { BABEL_DISABLE_CACHE: 1 },
    },
  }] },
  target: 'web',
  devtool: '#source-map',
};

gulp.task('app', () =>
  gulp.src(clientSrcJsPath)
  .pipe(webpackStream(webpackConfig))
  .pipe(gulp.dest(clientJsDestDir))
);
