'use strict';

const express = require('express');

const app = express();

// eslint-disable-next-line new-cap
const http = require('http').Server(app);
const io = require('socket.io')(http);

const port = process.env.PORT || 8080;
const base = `http://localhost:${port}/`;

const socketsPoole = require('./socketsPoole')(base);

app.get(
  '/bill_accepted/:socketId/:value',
  ({ params: { socketId, value } }, res) => {
    if (!socketId || !(value > 0)) return;
    socketsPoole.get(socketId).makeCash(value);

    res.send('');
  }
);

app.get(
  '/bill_declined/:socketId',
  ({ params: { socketId } }, res) => {
    if (!socketId) return;
    socketsPoole.get(socketId).declineCash();

    res.send('');
  }
);

io.on('connection', socket => {
  const session = socketsPoole.add(socket);

  console.log(`Подключен сокет ${socket.id}`);
  console.log(session.getAcceptUrl(100));
  console.log(session.getDeclineUrl());
});

app.set('views', './views');
app.set('view engine', 'pug');
app.use(express.static('build'));

app.get('/', (req, res) => res.render('index'));

http.listen(port, () => console.log(base));
