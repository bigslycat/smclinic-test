/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';
	
	document.addEventListener('DOMContentLoaded', function () {
	  var start = document.querySelector('.app__start-button');
	  var counter = document.querySelector('.app__counter');
	  var feed = document.querySelector('.app__feed');
	
	  start.addEventListener('click', function () {
	    var startClasses = start.getAttribute('class').split(/\s+/);
	    startClasses.push('button_hidden');
	    start.setAttribute('class', startClasses.join(' '));
	
	    var socket = io();
	
	    socket.on('connect', function () {
	      feed.innerHTML = 'Соединение успешно установлено';
	    });
	
	    var apply = void 0;
	
	    socket.on('cashMaked', function (value) {
	      counter.innerHTML = value;
	      feed.innerHTML = '';
	
	      if (value && typeof apply === 'undefined') {
	        apply = document.querySelector('.app__apply-button');
	        var applyClasses = apply.getAttribute('class').split(/\s+/);
	
	        apply.setAttribute('class', applyClasses.filter(function (currentClass) {
	          return currentClass !== 'button_hidden';
	        }).join(' '));
	
	        apply = null;
	      }
	    });
	
	    socket.on('cashDecline', function () {
	      feed.innerHTML = 'Ошибка распозанавания банкноты';
	    });
	  });
	});
	
	// new Promise(
	//   resolve => document.addEventListener('DOMContentLoaded', resolve)
	// ).then(() => new Promise(resolve => {
	//
	//   const start = document.querySelector('.app__start-button');
	//
	//   start.addEventListener('click', () => resolve(start));
	// })).then(result => {
	//   const { start } = result;
	//
	//   const startClasses = start.getAttribute('class').split(/\s+/);
	//   startClasses.push('button_hidden');
	//   start.setAttribute('class', startClasses.join(' '));
	//
	//   const counter = document.querySelector('.app__counter');
	//   const feed = document.querySelector('.app__feed');
	//
	//
	// });

/***/ }
/******/ ]);
//# sourceMappingURL=index.js.map