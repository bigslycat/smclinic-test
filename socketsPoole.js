'use strict';

const paySessionInit = require('./PaySession');

let socketsPoole;

module.exports = base => {
  const PaySession = paySessionInit(base);

  if (!socketsPoole) {
    socketsPoole = {
      sessions: {},

      add(socket) {
        const session = new PaySession(socket);
        this.sessions[socket.id.slice(2)] = session;

        return session;
      },

      get(socketId) {
        return this.sessions[socketId];
      },
    };
  }

  return socketsPoole;
};
