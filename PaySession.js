'use strict';

let PaySession;

module.exports = base => {
  if (!PaySession) {
    PaySession = class {
      constructor(socket) {
        this.socket = socket;
        this.cash = 0;
      }

      makeCash(amount) {
        this.cash += Number(amount);
        this.sendUpdate();

        return this.cash;
      }

      sendUpdate() {
        this.socket.emit('cashMaked', this.cash);
      }

      declineCash() {
        this.socket.emit('cashDecline');
      }

      getAcceptUrl(value) {
        return `${base}bill_accepted/${this.socket.id.slice(2)}/${value}`;
      }

      getDeclineUrl() {
        return `${base}bill_declined/${this.socket.id.slice(2)}`;
      }
    };
  }

  return PaySession;
};
